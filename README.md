### Tutorial for Angular Framework
ระบบเก็บข้อมูลผู้ใช้งานระบบ (User Management System)
- ค้นหาข้อมูลจาก รหัส, ชื่อ - นามสกุล
- เพิ่มข้อมูล
- แก้ไขข้อมูล
- ลบข้อมูล (เปลี่ยนสถานะเป็น Inactive)

Angular Features
- Module
- Component
- Service
- Directive
- Reactive Forms
- Pipes
- Routing (Eager, Lazy Load)
- HttpClient

Components
- UI (Ex. input, button) : Angular Material
- Layout : Angular Flex
- Datatable : Ngx Datatable

Angular Structure
- Smart Dump Components
- features/
  - components/
  - pages/
  - services/


### Tutorial for Spring Boot
- Rest Api Endpoint
  - ums/api/findByCriteria (POST)
    - Query by jpql
  - ums/api/create (POST)
  - ums/api/update (POST)
  - ums/api/inactive (PUT)
- DB : H2
- ORM : Hibernate
- Devtools
- Lombok
